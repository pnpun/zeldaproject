#ifndef BOMBER_H
#define BOMBER_H

#include "Enemy.h"

class Bomber : public Enemy
{
	public:
		Bomber();
		~Bomber();

		void update();
		void attack();
		void render();

		void getInputs();
		void renderAttack();
		void updateGraphic();

		void getRectangleAttack(std::vector <C_Rectangle> &attacks);

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Bomber";};

	protected:
	
	private:
		std::vector <Attack*> mpAttack;
		bool mpAttackMode;
		int mpCountdown;

		int mpGraphicID;

		ofSoundPlayer* mpExplosion;
	
};

#endif
