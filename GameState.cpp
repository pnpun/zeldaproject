//Include our classes
#include "GameState.h"

GameState::GameState(){
	mpRoom = " ";
	mpRooms.push_back('a');
}

GameState::~GameState(){
}

void GameState::update(){
	
}
void GameState::render(){
	
}

void GameState::saveGame() {
	int length = mpEntities.size();
	std::fstream file;
	std::string line;

	file.open("data/gamestate.save", std::ios::out | std::ios::binary);
	if (!file.is_open()) {
		exit(0);
	}
	else {
		file.write((char*) &length, sizeof(int));
		for (int i = 0; i < length; i++) {
			file.write((char*)&mpEntities[i], sizeof(Entities));
		}
	}
}

void GameState::reset() {
	mpRooms.clear();
	return;
}

// Setters and getters
void GameState::setPlayerInfo(int x, int y, int life) {
	player.life = life;
	player.x = x;
	player.y = y;
	return;
}

void GameState::getPlayerInfo(int &x, int &y, int &life) {
	life = player.life;
	x = player.x;
	y = player.y;
	return;
}

bool GameState::isOfClass(std::string classType){
	if(classType == "GameState"){
		return true;
	}
	return false;
}


bool GameState::newRoom(char c) {
	bool check = false;
	for (int i = 0; i < mpRooms.size(); i++) {
		if (mpRooms[i] == c) { return false; }
	}
	mpRooms.push_back(c);
	return true;
}