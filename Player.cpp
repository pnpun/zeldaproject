//Include our classes
#include "Player.h"

Player::Player() : Entity(){
	mpLife = 6;
	mpSpeed = 224;
	mpDamage = 1;
	mpMoving = false;
	mpAttack = new Attack();
	mpAttack->setEnemy(false);
	mpAttack->setDamage(mpDamage);
	mpAttackFrame = 0;
	mpAttackFrameTime = 0;

	mpGraphicID = sResManager->getGraphicID("img/player.png");

	mpHeartID = sResManager->getGraphicID("img/life.png");
	mpAttackID = sResManager->getGraphicID("img/attacks.png");

	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpHeartRect.w = TILE_SIZE;
	mpHeartRect.h = TILE_SIZE;
	mpHeartRect.x = 0;
	mpHeartRect.y = 0;


	mpAttackGraphicRect.w = TILE_SIZE;
	mpAttackGraphicRect.h = TILE_SIZE;
	mpAttackGraphicRect.x = 32;
	mpAttackGraphicRect.y = 32;
	mpAttacking = false;

	mpKeys = 0;

	mpAttackSFX = new ofSoundPlayer;
	mpAttackSFX->load("sound/Attack.wav");

	mpDamageSFX = new ofSoundPlayer;
	mpDamageSFX->load("sound/Damage.wav");
}

Player::~Player(){

}


void Player::update(){
	mpAttack->update();
	updateGraphic();
	if (mpLife <= 0) {
		mpAlive = false;
	}
	else {
		getInputs();
		if (!checkCollisionWithMap() && mpMoving) {
			move();
		}
		else {
			mpMoving = false;
		}
	}
	return;
}

void Player::render(){
	if (mpAlive) {
		ofSetColor(255, 255, 255);

		imgRender(mpGraphicID, mpCollisionRect.x, mpCollisionRect.y, mpGraphicRect);
		
		if (mpAttacking) { 
			if (mpDirection == UP) {
				imgRender(mpAttackID, mpCollisionRect.x, mpCollisionRect.y - 32, mpAttackGraphicRect);
			}
			else if (mpDirection == DOWN) {
				imgRender(mpAttackID, mpCollisionRect.x, mpCollisionRect.y + 32, mpAttackGraphicRect);
			} else if (mpDirection == LEFT) {
				imgRender(mpAttackID, mpCollisionRect.x - 32, mpCollisionRect.y , mpAttackGraphicRect);
			}
			else if (mpDirection == RIGHT) {
				imgRender(mpAttackID, mpCollisionRect.x + 32, mpCollisionRect.y , mpAttackGraphicRect);
			}
		}
		

		for (int i = 1; i <= mpLife; i++) {
			if (i % 2 == 0) {
				imgRender(mpHeartID, 16 + 16 * i, SCREEN_HEIGHT - 46, mpHeartRect);
			}
			if (mpLife % 2 == 1 && i == mpLife) {
				mpHeartRect.x = TILE_SIZE;
				imgRender(mpHeartID, 16 + 16 * i + 16, SCREEN_HEIGHT - 46, mpHeartRect);
			}
		}		
	}
}

void Player::getInputs() {
	if (mpMoving) {
		return;
	}
	if (key_down[' ']) {
		if (mpAttack->getCooldown() <= 0) {
			attack();
			mpAttack->setCooldown(750);
			mpAttacking = true;
		}
	}
	else if (key_down['w'] || key_down['W']) {
		mpDirection = UP;
		mpMoving = true;
	} else if (key_down['s'] || key_down['S']) {
		mpDirection = DOWN;
		mpMoving = true;
	} else if (key_down['a'] || key_down['A']) {
		mpDirection = LEFT;
		mpMoving = true;
	} else if (key_down['d'] || key_down['D']) {
		mpDirection = RIGHT;
		mpMoving = true;
	}

	updateControls();
	return;
}

void Player::updateControls() {
	x_to_go = mpCollisionRect.x;
	y_to_go = mpCollisionRect.y;

	switch (mpDirection) {
	case UP:
		x_to_go = mpCollisionRect.x;
		y_to_go = mpCollisionRect.y - TILE_SIZE;
		break;

	case DOWN:
		x_to_go = mpCollisionRect.x;
		y_to_go = mpCollisionRect.y + TILE_SIZE;
		break;

	case LEFT:
		x_to_go = mpCollisionRect.x - TILE_SIZE;
		y_to_go = mpCollisionRect.y;
		break;

	case RIGHT:
		x_to_go = mpCollisionRect.x + TILE_SIZE;
		y_to_go = mpCollisionRect.y;
		break;

	default:
		break;
	}
	return;
}

void Player::updateGraphic() {
	mpFrameTime += global_delta_time;
	if (mpFrameTime > 100) {
		mpFrameTime = 0;
		mpFrame++;
		if (mpFrame >= 4) {
			mpFrame = 0;
		}


	}

	if (mpAttacking) {
		mpAttackFrameTime += global_delta_time;
		if (mpAttackFrameTime > 20) {
			mpAttackFrameTime = 0;
			mpAttackFrame++;

			if (mpAttackFrame >= 5) {
				mpAttackFrame = 0;
				mpAttacking = false;
			}
		}
	}

	int row = mpDirection - 1;
	mpAttackGraphicRect.y = row * mpAttackGraphicRect.h;
	mpAttackGraphicRect.x = mpAttackFrame * mpAttackGraphicRect.w;
	mpGraphicRect.x = mpFrame * mpGraphicRect.w;
	mpGraphicRect.y = row * mpGraphicRect.h;
	mpHeartRect.x = 0;
	return;
}

//Attacks
void Player::attack() {
	int xx = mpCollisionRect.x;
	int yy = mpCollisionRect.y;
	if (mpDirection == UP) {
		yy -= TILE_SIZE;
	}
	else if (mpDirection == DOWN) {
		yy += TILE_SIZE;
	}
	else if (mpDirection == RIGHT) {
		xx += TILE_SIZE;
	}
	else if (mpDirection == LEFT) {
		xx -= TILE_SIZE;
	}
	mpAttack->setPosition(xx, yy);

	mpAttackSFX->play();
	return;
}

void Player::renderAttack() {
	mpAttack->render();
	return;
}

//-------------------------------------------
//			   SETTERS AND GETTERS
//-------------------------------------------
int Player::getDamage() {
	return mpDamage;
}
void Player::setDamage(int damage) {
	mpLife -= damage;

	mpDamageSFX->play();
	return;
}

bool Player::getAlive() {
	return mpAlive;
}
void Player::setAlive(bool alive) {
	mpAlive = alive;
	return;
}

int Player::getLives() {
	return mpLife;
}
void Player::setLives(int life) {
	mpLife = life;
	return;
}

C_Rectangle Player::getRectangleAttack() {
	return mpAttack->getRectangle();
}

bool Player::isOfClass(std::string classType){
	if(classType == "Player"){
		return true;
	}
	return false;
}

//-------------------------------------------
//			       GAMEPLAY
//-------------------------------------------
void Player::useKey() {
	mpKeys--;
	return;
}
void Player::addKey() {
	mpKeys++;
	return;
}
