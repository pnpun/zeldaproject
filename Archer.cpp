//Include our classes
#include "Archer.h"

Archer::Archer(){
	mpSpeed = 160;
	mpDirection = RIGHT;
	mpAlive = true;
	mpAttack = new Attack();
	mpAttack->setEnemy(true);
}

Archer::~Archer(){
}

void Archer::update(){
	Enemy::update();
	if (!mpAlive) { return; }

	getInputs();
	if (!checkCollisionWithMap()) {
		move();
	}
	else {
		mpMoving = false;
	}
	return;
	return;
}
void Archer::render(){
	if (mpAlive) {
		ofSetColor(255, 0, 255);
		ofDrawRectangle(mpCollisionRect.x, mpCollisionRect.y, TILE_SIZE, TILE_SIZE);
		mpAttack->render();
	}
}

void Archer::attack() {
}

void Archer::getInputs() {
	// If moving 'return'
	if (mpMoving) {
		return;
	}

	int playerPosX = mpPlayerDistanceX / TILE_SIZE;
	int playerPosY = mpPlayerDistanceY / TILE_SIZE;
	int posX = mpCollisionRect.x / TILE_SIZE;
	int posY = mpCollisionRect.y / TILE_SIZE;

	// Girar tenint en compte la seva direccio inicial
	// Detecta si ha d'atacar
	/*
	if (mpDirection == UP) {
		if (posY - 1 == playerPosY && posX == playerPosX) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY][posX + 1] && mpPlayerDistanceX > mpCollisionRect.x) {
			mpDirection = RIGHT;
		} else if (!(*mpCollisionMap)[posY][posX - 1] && mpPlayerDistanceX < mpCollisionRect.x) {
			mpDirection = LEFT;
		}
		else if ((*mpCollisionMap)[posY - 1][posX ]) {
			mpDirection = DOWN;
		}
	} else if (mpDirection == DOWN) {
		if (posY + 1 == playerPosY && posX == playerPosX) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY][posX + 1] && mpPlayerDistanceX > mpCollisionRect.x) {
			mpDirection = RIGHT;
		}
		else if (!(*mpCollisionMap)[posY][posX - 1] && mpPlayerDistanceX < mpCollisionRect.x) {
			mpDirection = LEFT;
		}
		else if ((*mpCollisionMap)[posY + 1][posX]) {
			mpDirection = RIGHT;
		}
	} else if (mpDirection == RIGHT) {
		if (posX + 1 == playerPosX && posY == playerPosY) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY + 1][posX] && mpPlayerDistanceY > mpCollisionRect.y) {
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY - 1][posX] && mpPlayerDistanceY < mpCollisionRect.y) {
			mpDirection = UP;
		}
		else if ((*mpCollisionMap)[posY][posX + 1]) {
			mpDirection = LEFT;
		}
	}
	else if (mpDirection == LEFT) {
		if (posX - 1 == playerPosX && posY == playerPosY) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY + 1][posX] && mpPlayerDistanceY > mpCollisionRect.y) {
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY - 1][posX] && mpPlayerDistanceY < mpCollisionRect.y) {
			mpDirection = UP;
		}
		else if ((*mpCollisionMap)[posY][posX - 1]) {
			mpDirection = RIGHT;
		}
	}
	*/

	if (mpDirection == UP) {
		if (playerPosY - 4 == posY && playerPosX == posX) {
			attack();
		}

	}
	else if (mpDirection == DOWN) {
		if (playerPosY + 4 == posY && playerPosX == posX) {
			attack();
		}
	}
	else if (mpDirection == LEFT) {
		if ( (playerPosX + 4 == posX || playerPosX - 4 == posX) && playerPosY == posY) {
			attack();
		}
		else if (playerPosX - 4 >= posX && playerPosY == posY) {
			mpDirection = RIGHT;
		}
	}
	else if (mpDirection == RIGHT) {
		if ( (playerPosX + 4 == posX || playerPosX - 4 == posX) && playerPosY == posY) {
			attack();
		}
		else if (playerPosX - 4 <= posX && playerPosY == posY) {
			mpDirection = LEFT;
		}
	}

	//TEST
	std::string direction = " ";
	if (mpDirection == LEFT) {
		direction = "left";
	}
	else if (mpDirection == RIGHT) {
		direction = "right";
	}
	else if (mpDirection == UP) {
		direction = "up";
	}
	else if (mpDirection == DOWN) {
		direction = "down";
	}
	std::cout << "Direction Archer: " << direction << std::endl;

	updateControls();
	return;
}

bool Archer::isOfClass(std::string classType){
	if(classType == "Archer" || classType == "Enemy"){
		return true;
	}
	return false;
}

