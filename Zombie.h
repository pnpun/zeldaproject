#ifndef ZOMBIE_H
#define ZOMBIE_H

#include "Enemy.h"

class Zombie : public Enemy
{
	public:
		Zombie();
		~Zombie();

		void update();
		void attack();
		void render();

		void getInputs();
		void renderAttack();
		void updateGraphic();

		C_Rectangle getRectangleAttack();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Zombie";};

	protected:
	
	private:
		Attack* mpAttack;
	
};

#endif
