#ifndef ARCHER_H
#define ARCHER_H

#include "Enemy.h"

class Archer : public Enemy
{
	public:
		Archer();
		~Archer();

		void update();
		void attack();
		void render();

		void getInputs();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "Archer";};

	protected:
	
	private:
		Attack* mpAttack;
};

#endif
