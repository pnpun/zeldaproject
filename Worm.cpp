//Include our classes
#include "Worm.h"

Worm::Worm() : Enemy(){
	mpSpeed = 272;
	mpDirection = NONE;
	mpLife = 1;
	mpAttack = new Attack();
	mpAttack->setEnemy(true);
	mpCooldown = 0;

	mpDamage = 1;

	// Eye
	mpEyeID = sResManager->getGraphicID("img/eye.png");
	mpEyeRect.x = 0;
	mpEyeRect.y = 0;
	mpEyeRect.h = TILE_SIZE;
	mpEyeRect.w = TILE_SIZE;

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpGraphicID = sResManager->getGraphicID("img/worm.png");

}

Worm::~Worm(){
}

void Worm::update(){
	updateGraphic();
	mpAttack->update();
	if (mpMoving) {
		mpEyeRect.x = TILE_SIZE;
	}
	else {
		mpEyeRect.x = 0;
	}
	Enemy::update();
	if (!mpAlive) { return; }
	
	getInputs();
	if (!checkCollisionWithMap()) {
		move();
	}
	else {
		mpMoving = false;
	}
	return;
}

void Worm::getInputs() {
	// If moving 'return'
	if (mpMoving) {
		return;
	}

	int playerPosX = mpPlayerDistanceX / TILE_SIZE;
	int playerPosY = mpPlayerDistanceY / TILE_SIZE;
	int posX = mpCollisionRect.x / TILE_SIZE;
	int posY = mpCollisionRect.y / TILE_SIZE;
	bool clear = true;
	int distance = 0;

	mpDirection = NONE;

	if (playerPosX == posX && posY == playerPosY && mpAttack->getCooldown() <= 0) {
		attack();
	}

	// Comprovar quan el jugador esta en linea
	// i no hi han obstacles pel mig
	
	if (playerPosX == posX) {
		// Comprovar Y
		distance = abs(posY - playerPosY);
		if (posY > playerPosY) {
			for (int i = 0; i < distance && clear; i++) {
				if ((*mpCollisionMap)[posY - i][posX]) {
					clear = false;
				}
			}
			if (clear) {
				y_to_go = playerPosY * TILE_SIZE;
				x_to_go = playerPosX * TILE_SIZE;
				mpMoving = true;
			}
		} else if (posY < playerPosY) {
			for (int i = 0; i < distance && clear; i++) {
				if ((*mpCollisionMap)[posY + i][posX]) {
					clear = false;
				}
			}
			if (clear) {
				y_to_go = playerPosY * TILE_SIZE;
				x_to_go = playerPosX * TILE_SIZE;
				mpMoving = true;
			}
		}
	} else if (playerPosY == posY) {
		// Comprovar X
		distance = abs(posX - playerPosX);
		if (posX < playerPosX) {
			for (int i = 0; i < distance && clear; i++) {
				if ((*mpCollisionMap)[posY][posX + i]) {
					clear = false;
				}
			}
			if (clear) {
				y_to_go = playerPosY * TILE_SIZE;
				x_to_go = playerPosX * TILE_SIZE;
				mpMoving = true;
			}
		}
		else if (posX > playerPosX) {
			for (int i = 0; i < distance && clear; i++) {
				if ((*mpCollisionMap)[posY][posX-i]) {
					clear = false;
				}
			}
			if (clear) {
				y_to_go = playerPosY * TILE_SIZE;
				x_to_go = playerPosX * TILE_SIZE;
				mpMoving = true;
			}
		}
	}
	return;
}

void Worm::attack() {
	int xx = mpCollisionRect.x;
	int yy = mpCollisionRect.y;
	mpAttack->setPosition(xx, yy);
	mpAttack->setCooldown(1000);
	mpAlive = false;
	return;
}

void Worm::render(){
	if (mpAlive) {
		ofSetColor(255, 255, 255);
		
		imgRender(mpGraphicID, mpCollisionRect.x, mpCollisionRect.y, mpGraphicRect);

		imgRender(mpEyeID, mpCollisionRect.x, mpCollisionRect.y - TILE_SIZE, mpEyeRect);
	}
}

void Worm::updateGraphic() {
	mpFrameTime += global_delta_time;
	if (mpFrameTime > 200) {
		mpFrameTime = 0;
		mpFrame++;
	}

	if (mpFrame >= 12) {
		mpFrame = 0;
	}

	mpGraphicRect.y = mpFrame * mpGraphicRect.h;
	return;
}

void Worm::renderAttack() {
	mpAttack->render();
	return;
}

C_Rectangle Worm::getRectangleAttack() {
	return mpAttack->getRectangle();
}

bool Worm::isOfClass(std::string classType){
	if(classType == "Worm" || classType == "Enemy"){
		return true;
	}
	return false;
}


