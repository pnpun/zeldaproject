//Include our classes
#include "Bomber.h"

Bomber::Bomber() : Enemy(){
	mpSpeed = 160;
	mpDirection = UP;
	mpDamage = 3;
	mpAttack.resize(9);
	for (int i = 0; i < 9; i++) {
		mpAttack[i] = new Attack();
	}
	mpAttackMode = false;
	mpCountdown = 1000;

	mpExplosion = new ofSoundPlayer;
	mpExplosion->load("sound/Explosion.wav");

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpFrameTime = 0;
	mpFrame = 0;

	mpGraphicID = sResManager->getGraphicID("img/bomb.png");
}

Bomber::~Bomber(){
}

void Bomber::update(){
	updateGraphic();
	if (mpAttackMode) {
		if (mpCountdown > 0) {
			mpCountdown -= global_delta_time;
		}
		else {
			attack();
		}
		return;
	}
	for (int i = 0; i < 9; i++) {
		mpAttack[i]->update();
	}

	Enemy::update();
	if (!mpAlive) { return; }

	getInputs();
	if (!checkCollisionWithMap()) {
		move();
	}
	else {
		mpMoving = false;
	}
	return;
}

void Bomber::getInputs() {
	// If moving or attacking 'return'
	if (mpMoving || mpAttackMode) {
		return;
	}

	int playerPosX = mpPlayerDistanceX / TILE_SIZE;
	int playerPosY = mpPlayerDistanceY / TILE_SIZE;
	int posX = mpCollisionRect.x / TILE_SIZE;
	int posY = mpCollisionRect.y / TILE_SIZE;

	// Detectar a l'enemic aprop
	if (abs(playerPosX - posX) < 2 && abs(playerPosY - posY) < 2) {
		mpAttackMode = true;
		return;
	}


	if (mpDirection == UP) {
		if (!(*mpCollisionMap)[posY][posX + 1] && mpPlayerDistanceX > mpCollisionRect.x) {
			mpDirection = RIGHT;
		} else if (!(*mpCollisionMap)[posY][posX - 1] && mpPlayerDistanceX < mpCollisionRect.x) {
			mpDirection = LEFT;
		}
		else if ((*mpCollisionMap)[posY - 1][posX ]) {
			mpDirection = DOWN;
		}
	} else if (mpDirection == DOWN) {
		if (!(*mpCollisionMap)[posY][posX + 1] && mpPlayerDistanceX > mpCollisionRect.x) {
			mpDirection = RIGHT;
		}
		else if (!(*mpCollisionMap)[posY][posX - 1] && mpPlayerDistanceX < mpCollisionRect.x) {
			mpDirection = LEFT;
		}
		else if ((*mpCollisionMap)[posY + 1][posX]) {
			mpDirection = RIGHT;
		}
	} else if (mpDirection == RIGHT) {
		if (!(*mpCollisionMap)[posY + 1][posX] && mpPlayerDistanceY > mpCollisionRect.y) {
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY - 1][posX] && mpPlayerDistanceY < mpCollisionRect.y) {
			mpDirection = UP;
		}
		else if ((*mpCollisionMap)[posY][posX + 1]) {
			mpDirection = LEFT;
		}
	}
	else if (mpDirection == LEFT) {
		if (!(*mpCollisionMap)[posY + 1][posX] && mpPlayerDistanceY > mpCollisionRect.y) {
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY - 1][posX] && mpPlayerDistanceY < mpCollisionRect.y) {
			mpDirection = UP;
		}
		else if ((*mpCollisionMap)[posY][posX - 1]) {
			mpDirection = RIGHT;
		}
	}

	updateControls();
	return;
}

void Bomber::attack() {
	int xx = mpCollisionRect.x;
	int yy = mpCollisionRect.y;
	mpAttack[0]->setPosition(xx, yy);
	mpAttack[1]->setPosition(xx, yy + TILE_SIZE);
	mpAttack[2]->setPosition(xx, yy - TILE_SIZE);
	mpAttack[3]->setPosition(xx + TILE_SIZE, yy);
	mpAttack[4]->setPosition(xx + TILE_SIZE, yy + TILE_SIZE);
	mpAttack[5]->setPosition(xx + TILE_SIZE, yy - TILE_SIZE);
	mpAttack[6]->setPosition(xx - TILE_SIZE, yy);
	mpAttack[7]->setPosition(xx - TILE_SIZE, yy + TILE_SIZE);
	mpAttack[8]->setPosition(xx - TILE_SIZE, yy - TILE_SIZE);
	mpAttackMode = false;
	//mpCollisionRect.x = 10000;
	//mpCollisionRect.y = 10000;
	mpSpeed = 0;
	mpAlive = 0;
	
	mpExplosion->play();
	return;
}

void Bomber::render(){
	if (mpAlive) {
		ofSetColor(255, 255, 255);

		imgRender(mpGraphicID, mpCollisionRect.x, mpCollisionRect.y, mpGraphicRect);

		ofSetColor(255, 0, 0);
		int life = mpCountdown / TILE_SIZE;
		for (int i = 0; i < life; i++) {
			ofDrawRectangle(mpCollisionRect.x + i, mpCollisionRect.y - 8, 1, 4);
		}
	}
}

void Bomber::renderAttack() {
	for (int i = 0; i < 9; i++) {
		mpAttack[i]->render();
	}
	return;
}

//Setters and getters
void Bomber::getRectangleAttack(std::vector <C_Rectangle> &attacks) {
	for (int i = 0; i < 9; i++) {
		attacks[i] = mpAttack[i]->getRectangle();
	}
	return;
}

bool Bomber::isOfClass(std::string classType){
	if(classType == "Bomber" || classType == "Enemy"){
		return true;
	}
	return false;
}

void Bomber::updateGraphic() {
	mpFrameTime += global_delta_time;
	if (mpFrameTime > 100) {
		mpFrameTime = 0;
		mpFrame++;
	}

	if (mpFrame >= 3) {
		mpFrame = 0;
	}

	mpGraphicRect.y = mpFrame * mpGraphicRect.h;
	return;
}