#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "Scene.h"
typedef struct {
	std::string type = " ";
	int life = 0;
	int x = 0;
	int y = 0;
	int keys = 0;
}Entities;

class GameState
{
	public:
		GameState();
		~GameState();

		void update();
		void render();
		void saveGame();
		void reset();

		void setPlayerInfo(int x, int y, int life);
		void getPlayerInfo(int &x, int &y, int &life);
		bool newRoom(char c);
		

		bool isOfClass(std::string classType);
		std::string getClassName(){return "GameState";};


	protected:
	
	private:
		Entities player;
		std::vector <char> mpRooms;
		std::vector <Entities> mpEntities;
		std::string mpRoom;
	
};

#endif
