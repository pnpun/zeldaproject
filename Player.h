#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"
#include "Attack.h"

class Player : public Entity
{
	public:
		Player();
		~Player();

		void init();
		void update();
		void render();
		void attack();
		void renderAttack();
		void useKey();
		void addKey();

		//Setters and getters
		C_Rectangle getRectangleAttack();

		bool getAlive();
		void setAlive(bool alive);
		
		int getLives();
		void setLives(int life);

		int getDamage();
		void setDamage(int damage);

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Player";};

	protected:
	
	private:
		void updateControls();
		void getInputs();
		void updateGraphic();

		int mpHeartID;
		int mpAttackID;
		C_Rectangle mpAttackGraphicRect;

		int mpDamage;
		int mpLife;
		int mpKeys;
		bool mpAttacking;
		int mpAttackFrame;
		int mpAttackFrameTime;

		Attack* mpAttack;

		ofSoundPlayer* mpAttackSFX;
		ofSoundPlayer* mpDamageSFX;
};

#endif
