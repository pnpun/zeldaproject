#include "singletons.h"

ResourceManager*	sResManager;
SceneDirector*		sDirector;
//GameState*			sGameState;

void instanceSingletons(){
	sResManager	  = ResourceManager::getInstance();
	sDirector	  = SceneDirector::getInstance();
	//sGameState	  = GameState::getInstance();
}