#ifndef ENEMY_H
#define ENEMY_H

#include "Entity.h"
#include "Attack.h"

class Enemy : public Entity
{
	public:
		Enemy();
		~Enemy();

		void update();
		void render();

		void setPlayerDistanceXY(int x, int y);
		
		int getDamage();
		void setDamage(int damage);

		virtual bool isOfClass(std::string classType);
		virtual std::string getClassName(){return "Enemy";};

	protected:
		int mpPlayerDistanceX;
		int mpPlayerDistanceY;
		int mpLife;

		int mpDamage;

		virtual void attack();
		void updateControls();

	private:
		

		
};

#endif
