#include "singletons.h"

//Include our classes
#include "SceneMenu.h"

SceneMenu::SceneMenu(): Scene(){ //Calls constructor in class Scene
}

SceneMenu::~SceneMenu(){
}

void SceneMenu::init(){
	Scene::init(); //Calls to the init method in class Scene
	mpLogoID = sResManager->getGraphicID("img/logo.png");
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpLogoID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpLogoID);

	mpFont_menu = new ofTrueTypeFont;
	mpFont_menu->loadFont("font/aa_menu.ttf", 32);
	mpContinue = false;

	mpWait = 0;
	mpOption = 0;

	ofColor col = ofGetBackgroundColor();
}

void SceneMenu::load(){
	Scene::load(); //Calls to the init method in class Scene
}

void SceneMenu::updateScene(){
	mpWait += global_delta_time;
	inputEvent();
	if (!mpContinue && mpOption <= 0) {
		mpOption = 1;
	}


	if (mpOption < 0) {
		mpOption = 0;
	} else if(mpOption > EXIT) {
		mpOption = EXIT;
	}
	
}

void SceneMenu::drawMenu() {
	ofSetColor(255,255,255);

	int pointHeight = 0;
	if (mpOption == 0) {
		pointHeight = 170;
	} else if (mpOption == 1) {
		pointHeight = 270;
	}
	else if (mpOption == 2) {
		pointHeight = 370;
	}

	ofDrawRectangle(332, pointHeight, 32, 32);

	mpFont_menu->drawString("New Game", 400, 300);
	mpFont_menu->drawString("Exit", 400, 400);
	if (!mpContinue) {
		ofSetColor(50, 50, 50);
	}
	mpFont_menu->drawString("Continue", 400, 200);

	
}

void SceneMenu::drawScene(){
	ofSetColor(255, 0, 0);
	imgRender(mpLogoID, SCREEN_WIDTH/2, 75, mpGraphicRect, 255);
	drawMenu();
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneMenu::inputEvent(){
	//if (key_released[13]) {	// ENTER
	//if (key_released[8]) {	// BACKSPACE
	//if (key_released[9]) {	// TAB
	//if (key_released[127]) {	// SUPR
	if (mpWait > 300) {
		if (key_down['w'] || key_down['W']) {
			mpOption--;
			mpWait = 0;
		}
		if (key_down['s'] || key_down['S']) {
			mpOption++;
			mpWait = 0;
		}

		if (key_down[13]) {
			if (mpOption == CONTINUE) {
				// Carregar partida
			}
			else if (mpOption == NEW_GAME) {
				sDirector->changeScene(SceneDirector::LEVEL); // ..., false); -> no vuelve a cargar
			}
			else if (mpOption == EXIT) {
				sDirector->exitGame();
			}
			mpWait = 0;
		}
	}
}