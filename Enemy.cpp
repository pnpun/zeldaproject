//Include our classes
#include "Enemy.h"

Enemy::Enemy() : Entity(){

	mpPlayerDistanceX = 0;
	mpPlayerDistanceY = 0;
	mpLife = 5;
}

Enemy::~Enemy(){
}

void Enemy::update(){
	if (mpLife <= 0) {
		mpAlive = false;
		mpCollisionRect.x = 0;
		mpCollisionRect.y = 0;
	}
}

void Enemy::attack() {

	return;
}

void Enemy::updateControls() {
	x_to_go = mpCollisionRect.x;
	y_to_go = mpCollisionRect.y;

	switch (mpDirection) {
	case UP:
		x_to_go = mpCollisionRect.x;
		y_to_go = mpCollisionRect.y - TILE_SIZE;
		mpMoving = true;
		break;

	case DOWN:
		x_to_go = mpCollisionRect.x;
		y_to_go = mpCollisionRect.y + TILE_SIZE;
		mpMoving = true;
		break;

	case LEFT:
		x_to_go = mpCollisionRect.x - TILE_SIZE;
		y_to_go = mpCollisionRect.y;
		mpMoving = true;
		break;

	case RIGHT:
		x_to_go = mpCollisionRect.x + TILE_SIZE;
		y_to_go = mpCollisionRect.y;
		mpMoving = true;
		break;

	default:
		break;
	}
	return;
}

void Enemy::render() {

}

//Setters and getters
void Enemy::setPlayerDistanceXY(int x, int y) {
	mpPlayerDistanceX = x;
	mpPlayerDistanceY = y;
}

int Enemy::getDamage() {
	return mpDamage;
}
void Enemy::setDamage(int damage) {
	mpLife -= damage;
	if (!mpMoving) {
		x_to_go = mpCollisionRect.x;
		y_to_go = mpCollisionRect.y;
	}
	// Where will the enemy go when you hit him
	switch (mpDirection) {
	case UP:
		mpCollisionRect.y -= 5;
		mpMoving = true;
		break;

	case DOWN:
		mpCollisionRect.y += 5;
		mpMoving = true;
		break;

	case LEFT:
		mpCollisionRect.x += 5;
		mpMoving = true;
		break;

	case RIGHT:
		mpCollisionRect.x -= 5;
		mpMoving = true;
		break;

	default:
		break;
	}
	return;
}

bool Enemy::isOfClass(std::string classType){
	if(classType == "Enemy"){
		return true;
	}
	return false;
}

