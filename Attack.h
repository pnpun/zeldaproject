#ifndef ATTACK_H
#define ATTACK_H

#include "ofApp.h"
#include "Entity.h"

class Attack : Entity
{
	public:
		Attack();
		~Attack();

		void update();
		void render();

		//Setters and getters
		void setPosition(int x, int y);		
		void getPosition(int &x, int &y);
		void resetPosition();

		bool getEnemy();
		void setEnemy(bool _mpEnemy);

		int getDamage();
		void setDamage(int damage);

		int getCooldown();
		void setCooldown(int cooldown);

		C_Rectangle getRectangle();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "Attack";};

	protected:
	
	private:
		C_Rectangle mpRect;
		bool mpEnemy;
		int mpDamage;
		int mpTimer;
		int mpCooldown;
};

#endif
