#ifndef SCENEMENU_H
#define SCENEMENU_H

#include "Scene.h"

enum Options { CONTINUE, NEW_GAME, EXIT};

//! SceneMenu class
/*!
	Handles the Scene for the main menu of the game.
*/
class SceneMenu : public Scene
{
	public:
		//! Constructor of an empty SceneMenu.
		SceneMenu();

		//! Destructor
		~SceneMenu();

		//! Initializes the Scene.
		virtual void init();

		//! Loads the scene (reinitializes)
		virtual void load();

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		virtual std::string getClassName(){return "SceneMenu";};

	protected:
		//! Updates the Scene
		void updateScene();

		//! Draws the Scene
		void drawScene();
	
		//! Takes keyboard input and performs actions
		virtual void inputEvent();

	private:
		void drawMenu();

		int			mpGraphicID;
		int			mpLogoID;
		
		int			mpOption;
		int			mpWait;

		bool		mpContinue;

		C_Rectangle	mpGraphicRect;
		ofTrueTypeFont* mpFont_menu;


};

#endif
