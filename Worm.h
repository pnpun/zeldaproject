#ifndef WORM_H
#define WORM_H

#include "Enemy.h"

class Worm : public Enemy
{
	public:
		Worm();
		~Worm();

		void update();
		void attack();
		void render();
		void renderAttack();
		void getInputs();

		C_Rectangle getRectangleAttack();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Worm";};

	protected:
	
	private:
		Attack* mpAttack;
		int mpCooldown;
		int mpEyeID;
		C_Rectangle mpEyeRect;

		void updateGraphic();
};

#endif
