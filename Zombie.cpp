//Include our classes
#include "Zombie.h"

Zombie::Zombie() : Enemy(){
	mpSpeed = 160;
	mpDirection = UP;
	mpAttack = new Attack();
	mpAttack->setEnemy(true);
	mpLife = 4;
	mpDamage = 1;

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;
	mpGraphicID = sResManager->getGraphicID("img/zombie.png");
	mpFrameTime = 0;
	mpFrame = 0;
}

Zombie::~Zombie(){
}

void Zombie::update(){
	if (!mpAlive) { return; }
	updateGraphic();
	mpAttack->update();
	Enemy::update();
	getInputs();
	if (!checkCollisionWithMap()) {
		move();
	}
	else {
		mpMoving = false;
	}
	return;
}

void Zombie::render() {
	if (mpAlive) {
		ofSetColor(255, 255, 255);
		imgRender(mpGraphicID, mpCollisionRect.x, mpCollisionRect.y, mpGraphicRect);
		ofSetColor(255, 0, 0);
		for (int i = 0; i < mpLife; i++) {
			ofDrawRectangle(mpCollisionRect.x + 1 + i * 8, mpCollisionRect.y - 8, 6, 4);
		}
	}
}

void Zombie::getInputs() {
	// If moving 'return'
	if (mpMoving) {
		return;
	}

	int playerPosX = mpPlayerDistanceX / TILE_SIZE;
	int playerPosY = mpPlayerDistanceY / TILE_SIZE;
	int posX = mpCollisionRect.x / TILE_SIZE;
	int posY = mpCollisionRect.y / TILE_SIZE;

	// Girar tenint en compte la seva direccio inicial
	// Detecta si ha d'atacar
	if (mpDirection == UP) {
		if (posY - 1 == playerPosY && posX == playerPosX) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY][posX + 1] && mpPlayerDistanceX > mpCollisionRect.x) {
			mpDirection = RIGHT;
		} else if (!(*mpCollisionMap)[posY][posX - 1] && mpPlayerDistanceX < mpCollisionRect.x) {
			mpDirection = LEFT;
		}
		else if ((*mpCollisionMap)[posY - 1][posX ]) {
			mpDirection = DOWN;
		}
	} else if (mpDirection == DOWN) {
		if (posY + 1 == playerPosY && posX == playerPosX) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY][posX + 1] && mpPlayerDistanceX > mpCollisionRect.x) {
			mpDirection = RIGHT;
		}
		else if (!(*mpCollisionMap)[posY][posX - 1] && mpPlayerDistanceX < mpCollisionRect.x) {
			mpDirection = LEFT;
		}
		else if ((*mpCollisionMap)[posY + 1][posX]) {
			mpDirection = RIGHT;
		}
	} else if (mpDirection == RIGHT) {
		if (posX + 1 == playerPosX && posY == playerPosY) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY + 1][posX] && mpPlayerDistanceY > mpCollisionRect.y) {
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY - 1][posX] && mpPlayerDistanceY < mpCollisionRect.y) {
			mpDirection = UP;
		}
		else if ((*mpCollisionMap)[posY][posX + 1]) {
			mpDirection = LEFT;
		}
	}
	else if (mpDirection == LEFT) {
		if (posX - 1 == playerPosX && posY == playerPosY) {
			if (mpAttack->getCooldown() <= 0) {
				attack();
				mpAttack->setCooldown(1000);
			}
			return;
		}
		if (!(*mpCollisionMap)[posY + 1][posX] && mpPlayerDistanceY > mpCollisionRect.y) {
			mpDirection = DOWN;
		}
		else if (!(*mpCollisionMap)[posY - 1][posX] && mpPlayerDistanceY < mpCollisionRect.y) {
			mpDirection = UP;
		}
		else if ((*mpCollisionMap)[posY][posX - 1]) {
			mpDirection = RIGHT;
		}
	}

	updateControls();
	return;
}

//Attack
void Zombie::attack() {
	int xx = mpCollisionRect.x;
	int yy = mpCollisionRect.y;
	if (mpDirection == UP) {
		yy -= TILE_SIZE;
	}
	else if (mpDirection == DOWN) {
		yy += TILE_SIZE;
	}
	else if (mpDirection == RIGHT) {
		xx += TILE_SIZE;
	}
	else if (mpDirection == LEFT) {
		xx -= TILE_SIZE;
	}
	mpAttack->setPosition(xx, yy);
	return;
}

void Zombie::renderAttack() {
	mpAttack->render();
	return;
}


C_Rectangle Zombie::getRectangleAttack() {
	return mpAttack->getRectangle();
}

bool Zombie::isOfClass(std::string classType){
	if(classType == "Zombie" || classType == "Enemy"){
		return true;
	}
	return false;
}

void Zombie::updateGraphic() {
	mpFrameTime += global_delta_time;
	if (mpFrameTime > 100) {
		mpFrameTime = 0;
		mpFrame++;
		if (mpFrame >= 4) {
			mpFrame = 0;
		}
	}
	int row = mpDirection - 1;
	mpGraphicRect.x = mpFrame * mpGraphicRect.w;
	mpGraphicRect.y = row * mpGraphicRect.h;
	return;
}