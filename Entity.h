#ifndef ENTITY_H
#define ENTITY_H

#include "ofApp.h"
#include "singletons.h"
#include "includes.h"

class Entity
{
	public:
		Entity();
		Entity(int x, int y);
		~Entity();

		virtual void update();
		virtual void render();

		virtual void move();
		virtual void updateGraphic();

		virtual void renderAttack();
		
		// Setters and getters
		void getXY(int &x, int &y);
		void setXY(int x, int y);

		int getX();
		void setX(int x);
		int getY();
		void setY(int y);

		int getSpeed();
		void setSpeed(int speed);

		C_Rectangle getRectangle();
		void setRectangle(C_Rectangle rect);

		void setCollisionMap(std::vector <std::vector<bool>>* _mpCollisionMap);
		
		virtual bool getAlive();
		virtual void setAlive(bool alive);
		
		virtual void setLives(int life);
		
		virtual int getDamage();
		virtual void setDamage(int damage);

		virtual void setPlayerDistanceXY(int x, int y);

		Direction getDirection();		
		
		virtual C_Rectangle getRectangleAttack();
		virtual void getRectangleAttack(std::vector <C_Rectangle> &attack);

		virtual bool isOfClass(std::string classType);
		virtual std::string getClassName(){return "Entity";};
		
	protected:
		int mpInitialX;
		int mpInitialY;
		C_Rectangle mpCollisionRect;
		bool mpAlive;
		std::vector <std::vector <bool>>* mpCollisionMap;

		// Movement
		int mpSpeed;
		int x_to_go;
		int y_to_go;
		bool mpMoving;
		bool checkCollisionWithMap();

		Direction mpDirection;

		// Graphics
		int mpGraphicID;

		int mpFrame;
		int mpFrameTime;

		C_Rectangle mpGraphicRect;
		C_Rectangle mpHeartRect;

	private:
	
};

#endif
