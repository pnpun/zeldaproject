#ifndef DOOR_H
#define DOOR_H

#include "Entity.h"

class Door : public Entity
{
	public:
		Door();
		~Door();

		void init(int x, int y, char link);
		void update();
		void render();
		char getLink();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Door";};

	protected:
	
	private:

		void updateGraphic();
		

		void calculateDir();

		Direction mpType;
		char mpLink;
	
};

#endif
