//Include our classes
#include "Entity.h"

Entity::Entity(){
	mpCollisionRect.x = 0;
	mpCollisionRect.y = 0;
	mpCollisionRect.w = TILE_SIZE;
	mpCollisionRect.h = TILE_SIZE;
	mpInitialX = 0;
	mpInitialY = 0;
	mpGraphicID = 0;
	mpSpeed = 0;
	x_to_go = 0;
	y_to_go = 0;
	mpFrame = 0;
	mpFrameTime = 0;
	mpDirection = UP;
	mpMoving = false;
	mpAlive = true;

}

Entity::Entity(int x, int y) {
	Entity();
	setXY(x, y);
	mpInitialX = x;
	mpInitialY = y;
}

Entity::~Entity(){
}

void Entity::update(){
	
}
void Entity::render(){
	
}

// Movement
void Entity::move() {
	if (mpMoving) {
		int x_aux = mpCollisionRect.x;
		int y_aux = mpCollisionRect.y;

		if (mpCollisionRect.x < x_to_go) {
			mpCollisionRect.x += mpSpeed * global_delta_time / 1000;
		}
		else if (mpCollisionRect.x > x_to_go) {
			mpCollisionRect.x -= mpSpeed * global_delta_time / 1000;
		}

		if (mpCollisionRect.y < y_to_go) {
			mpCollisionRect.y += mpSpeed * global_delta_time / 1000;
		}
		else if (mpCollisionRect.y > y_to_go) {
			mpCollisionRect.y -= mpSpeed * global_delta_time / 1000;
		}

		if ((x_aux < x_to_go && mpCollisionRect.x > x_to_go) || (x_aux > x_to_go && mpCollisionRect.x < x_to_go)) {
			mpCollisionRect.x = x_to_go;
		}
		if ((y_aux < y_to_go && mpCollisionRect.y > y_to_go) || (y_aux > y_to_go && mpCollisionRect.y < y_to_go)) {
			mpCollisionRect.y = y_to_go;
		}

		if (mpCollisionRect.x == x_to_go && mpCollisionRect.y == y_to_go) {
			mpMoving = false;
		}
	}
	return;
}

bool Entity::checkCollisionWithMap() {
	int xx = x_to_go / TILE_SIZE;
	int yy = y_to_go / TILE_SIZE;

	if (yy < 0 || yy >= (*mpCollisionMap).size()) {
		return true;
	}

	if (xx < 0 || xx >= (*mpCollisionMap)[0].size()) {
		return true;
	}

	return ((*mpCollisionMap)[yy][xx]);

}

// Graphics
void Entity::updateGraphic() {
	return;
}

void Entity::renderAttack() {
	return;
}

//-------------------------------------------
//			   SETTERS AND GETTERS
//-------------------------------------------
void Entity::getXY(int &x, int &y) {
	x = mpCollisionRect.x;
	y = mpCollisionRect.y;
	return;
}
void Entity::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

int Entity::getX() {
	return mpCollisionRect.x;
}
void Entity::setX(int x) {
	mpCollisionRect.x = x;
	return;
}
int Entity::getY() {
	return mpCollisionRect.y;
}
void Entity::setY(int y) {
	mpCollisionRect.y = y;
	return;
}

int Entity::getSpeed() {
	return mpSpeed;
}
void Entity::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}

C_Rectangle Entity::getRectangle() {
	return mpCollisionRect;
}
void Entity::setRectangle(C_Rectangle rect) {
	mpCollisionRect = rect;
	return;
}

void Entity::setCollisionMap(std::vector <std::vector<bool>>* _mpCollisionMap) {
	mpCollisionMap = _mpCollisionMap;
	return;
}

bool Entity::getAlive() {
	return mpAlive;
}
void Entity::setAlive(bool alive) { 
	mpAlive = alive;
	return; 
}

void Entity::setLives(int life) {
	return;
}

int Entity::getDamage() {
	return 0;
}
void Entity::setDamage(int damage) {
	return;
}

void Entity::setPlayerDistanceXY(int x, int y) {
	return;
}

Direction Entity::getDirection() {
	return mpDirection;
}

C_Rectangle Entity::getRectangleAttack() {
	C_Rectangle rect;
	rect.x = 0;
	rect.y = 0;
	rect.w = 0;
	rect.h = 0;
	return rect;
}

void Entity::getRectangleAttack(std::vector <C_Rectangle> &attack) {
	return;
}

bool Entity::isOfClass(std::string classType) {
	if (classType == "Entity") {
		return true;
	}
	return false;
}
