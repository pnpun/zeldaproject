//Include our classes
#include "Attack.h"

Attack::Attack(){
	mpRect.h = TILE_SIZE;
	mpRect.w = TILE_SIZE;
	mpRect.x = 0;
	mpRect.y = 0;
	mpEnemy = false;
	mpDamage = 0;
	mpTimer = 0;
}

Attack::~Attack(){
}

void Attack::update(){
	if (mpCooldown > 0) {
		mpCooldown -= global_delta_time;
	}

	resetPosition();
}
void Attack::render() {
		ofSetColor(255, 255, 0);
		ofDrawRectangle(mpRect.x, mpRect.y, TILE_SIZE, TILE_SIZE);
}

//-------------------------------------------
//			   SETTERS AND GETTERS
//-------------------------------------------
void Attack::getPosition(int &x, int &y) {
	x = mpRect.x;
	y = mpRect.y;
	return;
}
void Attack::setPosition(int x, int y) {
	mpTimer = 0;
	mpRect.x = x;
	mpRect.y = y;
	return;
}
void Attack::resetPosition() {
	mpRect.x = -32;
	mpRect.y = -32;
	return;
}

bool Attack::getEnemy() {
	return mpEnemy;
}
void Attack::setEnemy(bool _mpEnemy) {
	mpEnemy = _mpEnemy;
	return;
}

int Attack::getDamage() {
	return mpDamage;
}
void Attack::setDamage(int damage) {
	mpDamage = damage;
	return;
}

int Attack::getCooldown() {
	return mpCooldown;
}
void Attack::setCooldown(int cooldown) {
	mpCooldown = cooldown;
	return;
}

C_Rectangle Attack::getRectangle() {
	return mpRect;
}

bool Attack::isOfClass(std::string classType){
	if(classType == "Attack"){
		return true;
	}
	return false;
}

