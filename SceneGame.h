#ifndef SCENEGAME_H
#define SCENEGAME_H

#include "Scene.h"
#include <fstream>
#include "Player.h"
#include "Zombie.h"
#include "Bomber.h"
#include "Worm.h"
#include "Door.h"
#include "GameState.h"

// Size of the map
const int sizeX = 30;
const int sizeY = 15;

//! SceneGame class
/*!
	Handles the Scene for the levels of the game.
*/
class SceneGame : public Scene
{
	public:
		//! Constructor of an empty SceneMenu.
		SceneGame();

		//! Destructor
		~SceneGame();

		//! Initializes the Scene.
		virtual void init();

		//! Loads the scene (reinitializes)
		virtual void load();

		// Read the map file
		void readRoom(std::string roomFile);

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		virtual std::string getClassName(){return "SceneGame";};

	protected:
		//! Updates the Scene
		void updateScene();

		//! Draws the Scene
		void drawScene();
	
		//! Takes keyboard input and performs actions
		virtual void inputEvent();

	private:
		int mpZeldaID;
		int mpWallID;
		int mpWholeID;
		int mpFloorID;

		C_Rectangle mpGraphicRect;

		void drawRoom();
		void changeRoom(Direction dir, char link);

		Player* mpPlayer;
		Zombie* mpZombie;
		Bomber* mpBomber;
		Worm* mpWorm;
		GameState* mpGameState;

		int mpEnemiesToKill;
		int mpEnemiesAlive;

		Door* mpDoor;

		std::vector <std::vector <bool>> mpCollisionMap;
		std::vector <std::vector <char>> mpSpriteMap;

		std::vector <C_Rectangle> mpBomberAttacks;
		std::vector <Entity*> mpEntities;

};

#endif
