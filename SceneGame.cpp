#include "singletons.h"

//Include our classes
#include "SceneGame.h"

SceneGame::SceneGame() : Scene() { //Calls constructor in class Scene
}

SceneGame::~SceneGame() {
}

void SceneGame::init() {
	Scene::init(); //Calls to the init method in class Scene

	mpGameState = new GameState();

	mpCollisionMap.resize(sizeY);
	mpSpriteMap.resize(sizeY);
	for (int i = 0; i < sizeY; i++) {
		mpCollisionMap[i].resize(sizeX);
		mpSpriteMap[i].resize(sizeX);
	}
	mpBomberAttacks.resize(9);

	mpWallID = sResManager->getGraphicID("img/wall.png");
	mpWholeID = sResManager->getGraphicID("img/whole.png");
	mpFloorID = sResManager->getGraphicID("img/floor.png");

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h= TILE_SIZE;

	// Creates player in pos 0
	mpPlayer = new Player();
	mpPlayer->setXY(TILE_SIZE*3, TILE_SIZE*7);
	mpEntities.push_back(mpPlayer);
	mpEntities[0]->setCollisionMap(&mpCollisionMap);

	readRoom("aaRoom_a.txt");

}

void SceneGame::load() {
	Scene::load(); //Calls to the init method in class Scene
	if (mReload) {
		// loadMap("room1.txt");
	}
}

// Reads the file room
void SceneGame::readRoom(std::string roomFile) {
	mpEnemiesAlive = 0;
	mpEnemiesToKill = 0;
	std::fstream file;
	std::string line;
	char c = ' ';

	file.open(roomFile, std::ios::in);	// Tries to open the file

	if (!file.is_open()) {	// Checks if we couldn't open the file. If we can't then Exits the program
		sDirector->exitGame();
	}
	else {					// Reads the file
		for (int i = 0; i < sizeY; i++) {
			std::getline(file, line);
			for (int j = 0; j < sizeX; j++) {
				c = line[j];
				switch (c)
				{
				case '#':
					mpCollisionMap[i][j] = true;
					mpSpriteMap[i][j] = c;
					break;

				case 'P':
					mpCollisionMap[i][j] = false;
					mpSpriteMap[i][j] = c;
					break;
				case 'E':
					mpCollisionMap[i][j] = false;
					mpSpriteMap[i][j] = c;
					mpZombie = new Zombie();
					mpZombie->setCollisionMap(&mpCollisionMap);
					mpZombie->setXY(j*TILE_SIZE, i*TILE_SIZE);
					mpEntities.push_back(mpZombie);
					break;
				case '-':
					mpCollisionMap[i][j] = true;
					mpSpriteMap[i][j] = c;
					break;

				case 'B':
					mpCollisionMap[i][j] = false;
					mpSpriteMap[i][j] = c;
					mpBomber = new Bomber();
					mpBomber->setCollisionMap(&mpCollisionMap);
					mpBomber->setXY(j*TILE_SIZE, i*TILE_SIZE);
					mpBomber->getRectangleAttack(mpBomberAttacks);
					mpEntities.push_back(mpBomber);
					break;

				case 'W':
					mpCollisionMap[i][j] = false;
					mpSpriteMap[i][j] = c;
					mpWorm = new Worm();
					mpWorm->setCollisionMap(&mpCollisionMap);
					mpWorm->setXY(j*TILE_SIZE, i*TILE_SIZE);
					mpEntities.push_back(mpWorm);
					break;

				case 'a': case 'b': case 'c': case 'd': case 'e':
				{
					mpCollisionMap[i][j] = true;
					mpSpriteMap[i][j] = '#';
					mpDoor = new Door();
					mpDoor->init(j*TILE_SIZE, i*TILE_SIZE, c);
					mpEntities.push_back(mpDoor);
				}
				break;

				default:
					mpCollisionMap[i][j] = false;
					mpSpriteMap[i][j] = c;
					break;
				}
			}
		}
	}
	for (int i = 0; i < mpEntities.size(); i++) {
		if (mpEntities[i]->isOfClass("Enemy")) {
			mpEnemiesToKill++;
		}
	}

	return;
}

//-------------------------------------------
//			   DRAWING FUNCTIONS
//-------------------------------------------

// Draw the current room
void SceneGame::drawRoom() {
	char c = ' ';
	for (int i = 0; i < sizeY; i++) {
		for (int j = 0; j < sizeX; j++) {
			c = mpSpriteMap[i][j];
			switch (c) {
			case '#':
				ofSetColor(255,255,255);
				imgRender(mpWallID, j*TILE_SIZE, i*TILE_SIZE, mpGraphicRect);
				break;
			case '-':
				ofSetColor(255, 255, 255);
				imgRender(mpWholeID, j*TILE_SIZE, i*TILE_SIZE, mpGraphicRect);
				break;

			default:
				ofSetColor(255, 255, 255);
				imgRender(mpFloorID, j*TILE_SIZE, i*TILE_SIZE, mpGraphicRect);
				break;
			}
			
		}
	}
}

void SceneGame::updateScene() {
	bool check = false;
	if (!mpEntities[0]->getAlive()) {
		sDirector->goBack();
		mpEntities[0]->setAlive(true);
		mpEntities[0]->setLives(6);
		mpGameState->reset();
		changeRoom(NONE,'a');
		mpPlayer->setXY(TILE_SIZE * 3, TILE_SIZE * 7);
	}

	inputEvent();
	
	int length = 0;
	length = mpEntities.size();
	for (int i = 0; i < length; i++) {
		mpEntities[i]->setPlayerDistanceXY(mpEntities[0]->getX(), mpEntities[0]->getY());
		mpEntities[i]->update();
	}

	// Player's collision
	for (int i = 1; i < length; i++) {
		if (C_RectangleCollision( mpEntities[0]->getRectangleAttack(), mpEntities[i]->getRectangle() )) {
			mpEntities[i]->setDamage( mpEntities[0]->getDamage() );
		}
		if (C_RectangleCollision( mpEntities[0]->getRectangle(), mpEntities[i]->getRectangleAttack() )) {
			mpEntities[0]->setDamage( mpEntities[i]->getDamage() );
		}

		check = mpEntities[i]->isOfClass("Bomber");
		if (check) {
			mpEntities[i]->getRectangleAttack(mpBomberAttacks);
			for (int j = 0; j < 9; j++) {
				if (C_RectangleCollision(mpEntities[0]->getRectangle(), mpBomberAttacks[j])) {
					mpEntities[0]->setDamage( mpEntities[i]->getDamage() );
				}
			}
			check = false;
		}

		check = mpEntities[i]->isOfClass("Door");
		if (check) {
			if (C_RectangleTouch(mpEntities[i]->getRectangle(), mpEntities[0]->getRectangle()) //Collision
				&& mpEntities[0]->getDirection() == mpEntities[i]->getDirection() //Door & player = direction
				&& mpEntities[i]->getAlive() ) { //Doors is Alive
				changeRoom(mpEntities[i]->getDirection(), ((Door*)mpEntities[i])->getLink());
				return;
			}
		}
	}

	for (int i = 0; i < mpEntities.size(); i++) {
		if (mpEntities[i]->isOfClass("Enemy") && !mpEntities[i]->getAlive()) {
			mpEnemiesAlive++;
		}
	}
	if (mpEnemiesAlive >= mpEnemiesToKill) {
		for (int i = 0; i < mpEntities.size(); i++) {
			if (mpEntities[i]->isOfClass("Door")) {
				mpEntities[i]->setAlive(true);
			}
		}
	}
	else {
		mpEnemiesAlive = 0;
	}
}

void SceneGame::drawScene() {
	drawRoom();
	int length = mpEntities.size();
	for (int i = 0; i < length; i++) {
		mpEntities[i]->render();
	}
	for (int i = 0; i < length; i++) {
		mpEntities[i]->renderAttack();
	}
}

void SceneGame::changeRoom(Direction dir, char link) {

	// Save player's info
	int x = mpPlayer->getX();
	int y = mpPlayer->getY();
	int life = mpPlayer->getLives();
	mpGameState->setPlayerInfo(x, y, life);

	// Clear 
	FreeClear(mpEntities);

	// Remake player
	mpPlayer = new Player();
	mpGameState->getPlayerInfo(x, y, life);
	mpPlayer->setXY(x, y);
	mpPlayer->setLives(life);
	mpPlayer->setCollisionMap(&mpCollisionMap);
	mpEntities.push_back(mpPlayer);


	std::string document = "aaRoom_";
	document += link;
	document.append(".txt");

	readRoom(document);
	if (!mpGameState->newRoom(link)) {
		for (int i = 1; i < mpEntities.size(); i++) {
			if (mpEntities[i]->isOfClass("Enemy")) {
				mpEntities[i]->setLives(0);
				mpEntities[i]->setAlive(false);
			}
		}
	}

	
	if (dir == UP) {
		mpEntities[0]->setY(13*TILE_SIZE);
	}
	else if (dir == DOWN) {
		mpEntities[0]->setY(2 * TILE_SIZE);
	}else if (dir == LEFT) {
		mpEntities[0]->setX(28 * TILE_SIZE);
	}
	else if (dir == RIGHT) {
		mpEntities[0]->setX(1 * TILE_SIZE);
	}


	mLoaded = false;
	mReload = true;
	return;
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneGame::inputEvent() {
	if (key_released['b']) {
		sDirector->goBack();
	}
	if (key_down['l']) {
		for (int i = 0; i < mpEntities.size(); i++) {
			if (mpEntities[i]->isOfClass("Door")) {
				mpEntities[i]->setAlive(true);
			}
		}

	}
	if (key_down['p']) {
		readRoom("aaRoom_b.txt");
	}
}