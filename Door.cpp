//Include our classes
#include "Door.h"

Door::Door(){
	mpAlive = false;
	mpGraphicID = sResManager->getGraphicID("img/door.png");
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w= TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;
}

Door::~Door(){

}

void Door::init(int x, int y, char link) {
	setXY(x, y);
	mpLink = link;
	calculateDir();
	return;
}

void Door::update(){
	updateGraphic();
}

void Door::render(){
	imgRender(mpGraphicID, mpCollisionRect.x, mpCollisionRect.y, mpGraphicRect);
}

void Door::updateGraphic() {
	if (mpAlive) {
		if (mpGraphicRect.x == 0) {
			if (mpFrameTime < 150) { mpFrameTime += global_delta_time; }
			else {
				mpGraphicRect.x = 32;
				mpFrameTime = 0;
			}
		} else if (mpGraphicRect.x == 32) {
			if (mpFrameTime < 150) { mpFrameTime += global_delta_time; }
			else {
				mpGraphicRect.x = 64;
				mpFrameTime = 0;
			}
		}
	}
	if (mpDirection == DOWN) { mpGraphicRect.y = 32; }
	else if (mpDirection == LEFT) { mpGraphicRect.y = 64; }
	else if (mpDirection == RIGHT) { mpGraphicRect.y = 96; }
}

void Door::calculateDir() {
	if (mpCollisionRect.x < 64) {
		mpDirection = LEFT;
	}
	else if (mpCollisionRect.y < 64) {
		mpDirection = UP;
	}
	else if (mpCollisionRect.x > SCREEN_WIDTH - 64) {
		mpDirection = RIGHT;
	}
	else if (mpCollisionRect.y > 416) {
		mpDirection = DOWN;
	}
	return;
}

char Door::getLink() { return mpLink; }

bool Door::isOfClass(std::string classType){
	if(classType == "Door"){
		return true;
	}
	return false;
}

