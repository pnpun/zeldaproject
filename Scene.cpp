#include "singletons.h"

//Include our classes
#include "Scene.h"

Scene::Scene(){
	mLoaded = false;
	mReload = true;
}

Scene::~Scene(){
}

void Scene::init(){
	mLoaded = false;
	// Load images > resourceManager 
	return;
}

void Scene::load(){
	if(mReload){
		//Do a thing if re-entering
		//For example, reload a map
		//--------------------------
		// Poner al estado inicial
	}else{
		//Do a thing if not re-entering
		//--------------------------
		// 
	}
	mLoaded = true;
	return;
}

void Scene::onUpdate(){
	updateScene();
	return;
}

void Scene::onDraw(){
	if (!mLoaded) { return; }
	drawScene();
	return;
}

void Scene::inputEvent() {
	return;
}